package handle;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import model.FieldSelector;
import model.FillFieldDescription;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DesciptionReader {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static final Logger LOGGER = LogManager.getLogger(DesciptionReader.class);

  public static Map<String, FillFieldDescription> readFieldDescription(String path) {
    try {
      return MAPPER.readValue(DesciptionReader.class.getClassLoader().getResource(path),
          new TypeReference<Map<String, FillFieldDescription>>() {
          });
    } catch (Exception e) {
      LOGGER.error(e);
      throw new RuntimeException(e);
    }
  }

  public static Map<String, FieldSelector> readFieldSelector(String path) {
    try {
      return MAPPER.readValue(DesciptionReader.class.getClassLoader().getResource(path),
              new TypeReference<Map<String, FieldSelector>>() {
              });

    } catch (Exception e) {
      LOGGER.error(e);
      throw new RuntimeException(e);
    }
  }
}
