package handle;

import com.mifmif.common.regex.Generex;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import model.FieldDesc;
import model.FieldSelector;
import model.FillFieldDescription;
import model.Validator;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.CollectionUtils;

public class DefaultStepHandler implements Handler {

  private static final Logger LOGGER = LogManager.getLogger(DefaultStepHandler.class);

  private WebDriver driver;
  private Random random = new Random();
  private Map<String, FillFieldDescription> fillFieldDescriptionMap;
  private Map<String, FieldSelector> fieldNameToSelector;
  private Map<String, String> values = new HashMap<>();

  private boolean auto = false;

  public DefaultStepHandler(WebDriver driver, Map<String, FillFieldDescription> fillFieldDescriptionMap,
                            Map<String, FieldSelector> fieldNameToSelector) {
    this.driver = driver;

    this.fillFieldDescriptionMap = fillFieldDescriptionMap;
    this.fieldNameToSelector = fieldNameToSelector;
  }

  @Override
  public void handle() {

    fieldNameToSelector.entrySet().stream()
        .map(entry -> new SimpleEntry<>(entry.getKey(), getBy(entry.getValue())))
        .map(entry -> new SimpleEntry<>(entry.getKey(), findElement(entry.getValue())))
        .filter(entry -> entry.getValue() != null)
        .forEach(entry -> tryHandle(entry.getKey(), entry.getValue(),
            Optional.ofNullable(fillFieldDescriptionMap).map(map -> map.get(entry.getKey())).orElse(null)));
  }

  private void tryHandle(String fieldName, WebElement element, FillFieldDescription fillFieldDescription) {
    if ("submit".equals(fieldName) && !auto) {
      LOGGER.info("Skip submit button");
      return;
    }

    new WebDriverWait(driver, 10).until(d -> {
      boolean displayed = element.isDisplayed();
      if (!displayed) {
        return "0".equals(element.getCssValue("opacity"));
      }
      return displayed;
    });

    LOGGER.info("Try handle " + fieldName);
    String result = null;

    if (!element.isEnabled()) {
      LOGGER.info(fieldName + " is disabled - skip");
      return;
    }

    String initAction = Optional.ofNullable(fillFieldDescription).map(FillFieldDescription::getInitAction).orElse(null);
    if ("clear".equals(initAction)) {
      element.clear();
    } else if (StringUtils.isNotBlank(element.getAttribute("value")) && !"force".equals(initAction)) {
      LOGGER.info("Field " + fieldName + " already filled - skip");
      return;
    }

    if (Optional.ofNullable(element.getAttribute("type")).map(type -> type.equalsIgnoreCase("checkbox"))
        .orElse(false) || element.getTagName().equalsIgnoreCase("label")
        || element.getTagName().equalsIgnoreCase("button")) {
      LOGGER.info("Click on element " + fieldName);
      element.click();
      return;
    }

    LOGGER.info("Trying to get value from a user defined schema ");

    result = Optional.ofNullable(fillFieldDescription).map(this::getValueFromFillFieldDesc).orElse(null);
    String type = Optional.ofNullable(fillFieldDescription).map(FillFieldDescription::getType).orElse(null);
    if ("elastic".equalsIgnoreCase(type)) {
      logAndSend(result, fieldName, element);

      LOGGER.info("Trying handle elasticSearch field " + fieldName);
      new WebDriverWait(driver, 10).until(d -> {
        List<WebElement> elements = driver
            .findElements(By.cssSelector("mm-droplist[field-name=\"" + fieldName + "\"] ul a"));
        return !CollectionUtils.isEmpty(elements);
      });
      List<WebElement> elements = driver
          .findElements(By.cssSelector("mm-droplist[field-name=\"" + fieldName + "\"] ul a"));
      elements.get(random.nextInt(elements.size())).click();
      return;
    }

    if ("datepicker"
        .equalsIgnoreCase(type)) {
      LOGGER.info("Trying handle datepicker field " + fieldName);

      element.click();

      fillFieldDescription.getValues().forEach(value -> {
        List<WebElement> elements = driver.findElements(By.cssSelector(".picker-day"));
        elements.stream().filter(el -> value.contains(el.getText())).findFirst().ifPresent(WebElement::click);
      });

    }

    if (result != null) {
      if (element.getTagName().equalsIgnoreCase("select")) {
        Select select = new Select(element);
        if ("selectByText".equals(type)) {
          values.put(fieldName, result);
          select.selectByVisibleText(result);
        } else {
          values.put(fieldName, "string:" + result);
          select.selectByValue("string:" + result);
        }
      } else {
        logAndSend(result, fieldName, element);
      }
      return;
    }

    LOGGER.info("End handle " + fieldName);
  }


  private void logAndSend(String result, String fieldName, WebElement element) {
    values.put(fieldName, result);
    LOGGER.info(" send keys " + result + " for fieldName " + fieldName);
    element.sendKeys(result);

  }

  private String getValueFromFillFieldDesc(FillFieldDescription description) {
    String type = description.getType();
    if (!StringUtils.isNotBlank(type)) {
      return null;
    }

    if (type.trim().equalsIgnoreCase("generated")) {
      if (StringUtils.isNotBlank(description.getValue())) {
        return new Generex(description.getValue()).random();
      }
      return null;
    }

    if (type.trim().equalsIgnoreCase("select") || type.trim().equalsIgnoreCase("selectByText")) {
      if (!CollectionUtils.isEmpty(description.getValues())) {
        return description.getValues().get(random.nextInt(description.getValues().size()));
      }
      return null;
    }

    if (type.trim().equalsIgnoreCase("field")) {
      if (StringUtils.isNotBlank(description.getValue())) {
        return values.get(description.getValue());
      }
      return null;
    }

    if (type.trim().equalsIgnoreCase("elastic")) {
      if (StringUtils.isNotBlank(description.getValue())) {
        return description.getValue();
      }
      return null;
    }

    return description.getValue();
  }

  private By getBy(FieldSelector fieldSelector) {
    By by = Optional.ofNullable(fieldSelector).map(FieldSelector::getId).map(By::id).orElse(null);
    if (by == null) {
      by = Optional.ofNullable(fieldSelector).map(FieldSelector::getCss).map(By::cssSelector).orElse(null);
    }
    return by;
  }

  private WebElement findElement(By by) {
    try {
      return driver.findElement(by);
    } catch (Exception e) {
      e.printStackTrace();
      LOGGER.info("element by  = " + by + " not found");
      LOGGER.info(e.getMessage());
    }
    return null;
  }

  private String fillField(FieldDesc desc) {
    List<Validator> validators = Optional.of(desc)
        .map(FieldDesc::getValidators)
        .orElseGet(Collections::emptyList);
/*
    boolean required = validators.stream()
        .anyMatch(validator -> validator.getType().equals("required"));
*/
    Validator regExp = validators.stream().filter(validator -> validator.getType().equals("regExp"))
        .findFirst().orElse(null);

    if (regExp != null) {
      return new Generex(regExp.getValue()).random();
    }

    return null;
  }

  public boolean isAuto() {
    return auto;
  }

  public DefaultStepHandler setAuto(boolean auto) {
    this.auto = auto;
    return this;
  }
}
