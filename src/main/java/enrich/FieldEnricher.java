package enrich;

import java.util.List;
import model.Validator;

public interface FieldEnricher {

  String enrich(Validator validator);

}
