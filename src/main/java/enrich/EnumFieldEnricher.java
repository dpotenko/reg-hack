package enrich;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import model.Validator;

public class EnumFieldEnricher implements FieldEnricher {

  @Override
  public String enrich(Validator validator) {
    List<String> enums = Optional.of(validator).map(Validator::getOneOf).orElseGet(Collections::emptyList);

    if (enums.size() > 0) {
      return enums.get(0);
    } else {
      return null;
    }
  }
}
