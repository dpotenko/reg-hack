package start;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import model.EventRequestParam;
import model.Payload;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.filters.ResponseFilter;
import net.lightbody.bmp.filters.ResponseFilterAdapter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;

public class Proxy {

  private final static org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Proxy.class);

  private String releaseUrlForCatch = "https://qa-delivery-mx-release.moneyman.ru/client-area/#/registration";
  private String prodUrlCatch = "https://www.moneyman.com.mx/client-area/#/registration";
  private String urlForCatch = "https://qa-delivery-mx-master.moneyman.ru/client-area/#/registration";

  private String baseUrlTest = "https://qa-delivery-mx-master.moneyman.ru/";
  private String baseUrlRelease = "https://qa-delivery-mx-release.moneyman.ru/";
  private String baseUrlProd = "https://www.moneyman.com.mx/";


  private Map<Integer, Start> cookieToStart = new HashMap<>();
  private Map<String, Start> instToStart = new HashMap<>();
  private final org.openqa.selenium.Proxy seleniumProxy;

  public static Proxy INSTANCE;

  public Proxy() {

    ObjectMapper mapper = new ObjectMapper();
    BrowserMobProxy proxy = new BrowserMobProxyServer();
    proxy.setTrustAllServers(true);
    proxy.start(0);

    seleniumProxy = ClientUtil.createSeleniumProxy(proxy);

    ResponseFilter filter = new ResponseFilter() {
      @Override
      @SneakyThrows
      public void filterResponse(HttpResponse response, HttpMessageContents contents, HttpMessageInfo messageInfo) {

        String originalUrl = messageInfo.getOriginalUrl();

        if ((originalUrl.equalsIgnoreCase(urlForCatch) ||
            originalUrl.equalsIgnoreCase(releaseUrlForCatch) ||
            originalUrl.equalsIgnoreCase(prodUrlCatch)) &&
            messageInfo.getOriginalRequest().getMethod().equals(HttpMethod.GET)) {
          System.out.println(originalUrl);

          Payload payload = mapper.readValue(contents.getTextContents(), Payload.class);
          String threadId = Stream.of(messageInfo.getOriginalRequest().headers().get("Cookie").split(";"))
              .filter(c -> c.contains("threadId")).findFirst().get().replace("threadId=", "").trim();

          cookieToStart.get(Integer.valueOf(threadId))
              .pushTask(payload.getResponse().getName(), payload.getResponse());
        }
      }
    };
    RequestFilter requestFilter = new RequestFilter() {
      @Override
      @SneakyThrows
      public HttpResponse filterRequest(HttpRequest request, HttpMessageContents contents,
          HttpMessageInfo messageInfo) {
        String originalUrl = messageInfo.getOriginalUrl();

        if ((originalUrl.equalsIgnoreCase(urlForCatch) ||
            originalUrl.equalsIgnoreCase(releaseUrlForCatch) ||
            originalUrl.equalsIgnoreCase(prodUrlCatch))) {
          if (StringUtils.isBlank(request.headers().get("Authorization"))) {
            request.headers().add("Authorization", "Basic bW9uZXltYW46MTAwNQ==");
          }
        }

        if (originalUrl.equalsIgnoreCase(baseUrlProd + "mx-registration/actions/instantor-save-event") ||
            originalUrl.equalsIgnoreCase(baseUrlTest + "mx-registration/actions/instantor-save-event") ||
            originalUrl.equalsIgnoreCase(baseUrlRelease + "mx-registration/actions/instantor-save-event")) {
          EventRequestParam eventRequestParam = mapper.readValue(contents.getTextContents(), EventRequestParam.class);

          Optional.ofNullable(eventRequestParam)
              .filter(param -> Optional.ofNullable(param.getEventBody()).map(body -> body.get("status"))
                  .map(JsonNode::asBoolean).orElse(Boolean.FALSE))
              .map(uid -> {
                String instantorRequestUid = uid.getInstantorRequestUid();
                Start start = instToStart.get(instantorRequestUid);
                instToStart.remove(instantorRequestUid);
                LOGGER.info("Trying to get start for request id " + instantorRequestUid + " result = " + start);
                return start;
              })
              .ifPresent(start -> start.pushTask("INSTANTOR_LOGIN", null));
        }
        return null;
      }
    };
    proxy.addRequestFilter(requestFilter);
    proxy.addLastHttpFilterFactory((new ResponseFilterAdapter.FilterSource(filter, 16777216)));
  }

  public org.openqa.selenium.Proxy getSeleniumProxy() {
    return seleniumProxy;
  }

  public void addStart(Start start, int id) {
    cookieToStart.put(id, start);
  }

  public void addAcceptedPolicyStart(String requestUid, Start start) {
    LOGGER.info("add start with uid = " + requestUid);
    instToStart.put(requestUid, start);
  }

  public void deleteAcceptedPolicyStart(String requestUid) {
    LOGGER.info("delete start with uid = " + requestUid);
    instToStart.remove(requestUid);
  }

}
