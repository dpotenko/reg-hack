package start;

import static handle.DesciptionReader.readFieldDescription;
import static handle.DesciptionReader.readFieldSelector;

import handle.DefaultStepHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import model.FillFieldDescription;
import model.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Start {

  public static final String RELEASE = "https://moneyman:1005@qa-delivery-mx-release.moneyman.ru/registration/static";
  public static final String PROD_URL = "https://www.moneyman.com.mx/registration/static";
  public static final String TEST = "https://moneyman:1005@qa-delivery-mx-master.moneyman.ru/registration/static";
  private static final Logger LOGGER = LogManager.getLogger(Start.class);
  private static Map<String, BlockingQueue<Runnable>> cookieToTask = new HashMap<>();
  private final String testCookieUrl = "https://moneyman:1005@qa-delivery-mx-master.moneyman.ru/";
  private final String releaseCookieUrl = "https://moneyman:1005@qa-delivery-mx-release.moneyman.ru/";
  private final String prodCookieUrl = "https://www.moneyman.com.mx/";
  private final int id;
  private String cookieUrl = testCookieUrl;
  private String prefix = "";
  private BlockingQueue<Runnable> blockingQueue;
  private DefaultStepHandler step1;
  private DefaultStepHandler step2;
  private DefaultStepHandler step3;
  private DefaultStepHandler step4;
  private DefaultStepHandler step5;
  private DefaultStepHandler step6;
  private DefaultStepHandler instantor1;
  private DefaultStepHandler instantor2;
  private WebDriver driver;
  private String url = TEST;
  private String currentStep;

  private String requestUid;


  public Start(Boolean withInstantor, int stepNum, int env, org.openqa.selenium.Proxy proxy, int id) {
    this.id = id;
    System.setProperty("webdriver.chrome.driver",
        System.getProperty("driver", "C:\\soft\\chromedriver\\chromedriver.exe"));

    blockingQueue = new ArrayBlockingQueue<>(10, true);

    ChromeOptions chromeOptions = new ChromeOptions();
    chromeOptions.setProxy(proxy);
    chromeOptions.setAcceptInsecureCerts(true);
    chromeOptions.addArguments("ignore-certificate-errors");
    driver = new ChromeDriver(chromeOptions);

    if (env == 1) {
      url = RELEASE;
      cookieUrl = releaseCookieUrl;
    } else if (env == 2) {
      url = PROD_URL;
      prefix = "Yannick/";
      cookieUrl = prodCookieUrl;
    }

    step1 = new DefaultStepHandler(driver,
        readFieldDescription(prefix + "step1/step1.json"),
        readFieldSelector(prefix + "step1/step1-selectors.json"));

    step2 = new DefaultStepHandler(driver,
        readFieldDescription(prefix + "step2/step2.json"),
        readFieldSelector(prefix + "step2/step2-selectors.json"));

    Map<String, FillFieldDescription> step3Desc = readFieldDescription(prefix + "step3/step3.json");
    Map<String, FillFieldDescription> step3DescWithInst = readFieldDescription(
        "correction/instantor/with/step3/step3.json");

    Map<String, FillFieldDescription> step4Desc = readFieldDescription(prefix + "step4/step4.json");
    Map<String, FillFieldDescription> step4DescWithInst = readFieldDescription(
        "correction/instantor/with/step4/step4.json");

    if (withInstantor && env != 2) {
      step3Desc.putAll(step3DescWithInst);
      step4Desc.putAll(step4DescWithInst);
    }

    step3 = new DefaultStepHandler(driver, step3Desc,
        readFieldSelector(prefix + "step3/step3-selectors.json"));
    step4 = new DefaultStepHandler(driver, step4Desc,
        readFieldSelector(prefix + "step4/step4-selectors.json"));

    step5 = new DefaultStepHandler(driver,
        readFieldDescription(prefix + "step5/step5.json"),
        readFieldSelector(prefix + "step5/step5-selectors.json"));

    step6 = new DefaultStepHandler(driver,
        readFieldDescription(prefix + "step6/step6.json"),
        readFieldSelector(prefix + "step6/step6-selectors.json"));

    instantor1 = new DefaultStepHandler(driver,
        readFieldDescription(prefix + "instantor/policy/instantor.json"),
        readFieldSelector(prefix + "instantor/policy/instantor-selectors.json"));

    instantor2 = new DefaultStepHandler(driver,
        readFieldDescription(prefix + "instantor/fake/instantor.json"),
        readFieldSelector(prefix + "instantor/fake/instantor-selectors.json"));

    switch (stepNum) {
      case 6:
        step6.setAuto(true);
      case 5:
        step5.setAuto(true);
      case 4:
        step4.setAuto(true);
      case 3:
        step3.setAuto(true);
      case 2:
        step2.setAuto(true);
      case 1:
        step1.setAuto(true);
      default:
        break;
    }
  }

  public void start() {
    Cookie ck = new Cookie("threadId", String.valueOf(id));
    driver.get(url);
    driver.manage().addCookie(ck);
    driver.get(url);

    while (!Thread.currentThread().isInterrupted()) {
      Runnable runnable;
      try {
        runnable = blockingQueue.poll(20, TimeUnit.SECONDS);
        if (runnable != null) {
          LOGGER.info(Thread.currentThread().getName() + " run new task");
          Thread thread = new Thread(runnable);
          Thread.sleep(600);
          thread.start();
          thread.join();
        }

      } catch (InterruptedException e) {
        LOGGER.info(Thread.currentThread().getName() + " is interrupted");
        break;
      } catch (Exception e) {
        e.printStackTrace();
        break;
      }
    }

    LOGGER.info("driver quit");
    driver.quit();
  }

  public void pushTask(String stepName, Response response) {

    switch (stepName) {
      case "FILL_PERSONAL_DATA": {
        blockingQueue.add(step2::handle);
        break;
      }
      case "FILL_PERSONAL_DATA_EXP_AB_MMMX_1673_C": {
        blockingQueue.add(step2::handle);
        break;
      }
      case "CREATE_USER_ACCOUNT": {
        blockingQueue.add(step1::handle);
        break;
      }
      case "FILL_EMPLOYMENT_AND_INCOME": {
        blockingQueue.add(step3::handle);
        break;
      }
      case "CHOOSE_TRANSFER_SOURCE": {
        blockingQueue.add(step4::handle);
        break;
      }
      case "CHOOSE_TRANSFER_SOURCE_AB_MMMX_3125_B": {
        blockingQueue.add(step4::handle);
        break;
      }
      case "CALCULATOR": {
        blockingQueue.add(step5::handle);
        break;
      }
      case "UPLOAD_DOCUMENTS_BANK_STATEMENT_STEP": {
        blockingQueue.add(step6::handle);
        break;
      }
      case "UPLOAD_DOCUMENTS": {
        blockingQueue.add(step6::handle);
        break;
      }
      case "INSTANTOR_AB_MMMX_3125_B":
      case "INSTANTOR": {
        if (currentStep != null) {
          LOGGER.info("currentStep=" + currentStep + " - skip");
          return;
        }
        currentStep = "INSTANTOR";
        blockingQueue.add(() -> new WebDriverWait(driver, 10)
            .until(ExpectedConditions.visibilityOfElementLocated((By.id("instantor")))));
        blockingQueue.add(() -> driver.switchTo().frame(driver.findElement(By.id("instantor"))));
        blockingQueue.add(() -> {
          instantor1.handle();
          String instantorRequestUid = (String) response.getFields().get("instantorRequestUid");
          acceptConsent(instantorRequestUid);
          Proxy.INSTANCE.addAcceptedPolicyStart(instantorRequestUid, this);
          currentStep = null;
        });
        break;
      }
      case "INSTANTOR_LOGIN": {
        blockingQueue.add(() -> {
          instantor2.handle();
          currentStep = null;
        });
        break;
      }
      default:
        break;
    }
  }

  private void acceptConsent(String requestUid) {
    this.requestUid = requestUid;
  }

}
