package log;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javafx.scene.control.TextArea;

public class TextAreaWriter extends OutputStream {

  public static TextArea textArea;

  private StringBuffer stringBuffer = new StringBuffer();

  PublishSubject<String> subject = PublishSubject.<String>create();

  public TextAreaWriter() {
    Observable
        .defer(() -> subject)
        .buffer(500, TimeUnit.MILLISECONDS)
        .doOnNext(str -> Optional.ofNullable(textArea)
            .ifPresent(area -> str.stream().reduce(String::concat).ifPresent(area::appendText)))
        .subscribe();
  }

  @Override
  public void write(int b) throws IOException {
    stringBuffer.append(String.valueOf(Character.valueOf((char) b)));
  }

  @Override
  public void flush() throws IOException {
    synchronized (stringBuffer) {
      subject.onNext(stringBuffer.toString());
      stringBuffer.delete(0, stringBuffer.length());
    }
  }
}
