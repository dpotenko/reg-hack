package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class FillFieldDescription {

  @JsonProperty("value")
  private String value;

  @JsonProperty("type")
  private String type;

  @JsonProperty("initAction")
  private String initAction;

  @JsonProperty("values")
  private List<String> values;

  public FillFieldDescription() {
  }

  public FillFieldDescription(String value, String type, String initAction, List<String> values) {
    this.value = value;
    this.type = type;
    this.initAction = initAction;
    this.values = values;
  }

  public String getValue() {
    return value;
  }

  public FillFieldDescription setValue(String value) {
    this.value = value;
    return this;
  }

  public String getType() {
    return type;
  }

  public FillFieldDescription setType(String type) {
    this.type = type;
    return this;
  }

  public String getInitAction() {
    return initAction;
  }

  public FillFieldDescription setInitAction(String initAction) {
    this.initAction = initAction;
    return this;
  }

  public List<String> getValues() {
    return values;
  }

  public FillFieldDescription setValues(List<String> values) {
    this.values = values;
    return this;
  }
}
