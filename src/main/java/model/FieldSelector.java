package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FieldSelector {

  @JsonProperty("id")
  private String id;

  @JsonProperty("css")
  private String css;

  public FieldSelector(String id, String css) {
    this.id = id;
    this.css = css;
  }

  public FieldSelector() {
  }

  public String getId() {
    return id;
  }

  public FieldSelector setId(String id) {
    this.id = id;
    return this;
  }

  public String getCss() {
    return css;
  }

  public FieldSelector setCss(String css) {
    this.css = css;
    return this;
  }
}
