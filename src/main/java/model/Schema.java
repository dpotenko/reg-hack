package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

public class Schema {

  private  Map<String, FieldDesc> fieldDescs;

  public Schema() {
  }

  public Schema(Map<String, FieldDesc> fieldDescs) {
    this.fieldDescs = fieldDescs;
  }

  public Map<String, FieldDesc> getFieldDescs() {
    return fieldDescs;
  }

  @Override
  public String toString() {
    return "Schema{" +
        "fieldDescs=" + fieldDescs +
        '}';
  }
}
