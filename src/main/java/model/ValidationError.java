package model;

import static java.util.Optional.ofNullable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import one.util.streamex.StreamEx;

/**
 * Return this object after validation if error was occurred.
 */
public class ValidationError {

  /**
   * Subject which occurred error
   */
  private final String subject;
  /**
   * Info for system about error
   */
  private final String systemMessage;
  /**
   * Key for ResourceBundle
   */
  private final String code;
  /**
   * Message for user (optional)
   */
  private final String userMessage;
  private final List<String> params;

  public ValidationError(String subject, String userMessage) {
    this.subject = subject;
    this.userMessage = userMessage;
    this.systemMessage = null;
    this.code = null;
    this.params = Collections.emptyList();
  }

  @JsonCreator
  public ValidationError(
      @JsonProperty("subject") String subject,
      @JsonProperty("systemMessage") String systemMessage,
      @JsonProperty("code") String code,
      @JsonProperty("userMessageCode") String userMessageCode
  ) {
    this.subject = subject;
    this.code = code;
    this.userMessage = userMessageCode;
    this.systemMessage = ofNullable(systemMessage).orElse(userMessageCode);
    this.params = Collections.emptyList();
  }

  /**
   * Crates object with correct message from validation-messages.properties
   *
   * @param userMessageCode Key for ResourceBundle
   */
  public ValidationError(String subject, String systemMessage, String userMessageCode, Object... params) {
    this.subject = subject;
    this.code = userMessageCode;
    this.userMessage = ofNullable(userMessageCode)
        .map(m -> getMessageByCode(m, params))
        .orElse("");
    this.systemMessage = ofNullable(systemMessage).orElse(userMessageCode);
    this.params = StreamEx.of(params).filter(java.util.Objects::nonNull).map(String::valueOf).toList();
  }

  /**
   * Creates list of one ValidationError(use it in com.mm.base.modelValidator)
   */
  public static List<ValidationError> errors(String subject, String code, Object... params) {
    return Collections.singletonList(error(subject, code, params));
  }

  /**
   * Creates single ValidationError
   */
  public static ValidationError error(String subject, String code, Object... params) {
    return new ValidationError(subject, code, code, params);
  }

  private static String getMessageByCode(String code, Object[] params) {
    try {
      ResourceBundle bundle = ResourceBundle.getBundle("validation-messages", Locale.getDefault());
      return String.format(bundle.getString(code), params);
    } catch (MissingResourceException e) {
      return code;
    }
  }

  public String getSubject() {
    return subject;
  }

  public String getUserMessage() {
    return userMessage;
  }

  public String getSystemMessage() {
    return systemMessage;
  }

  public String getCode() {
    return code;
  }

  public List<String> getParams() {
    return params;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(subject, systemMessage, userMessage);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ValidationError that = (ValidationError) o;
    return Objects.equal(subject, that.subject) &&
        Objects.equal(systemMessage, that.systemMessage) &&
        Objects.equal(code, that.code) &&
        Objects.equal(userMessage, that.userMessage);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("subject", subject)
        .add("code", code)
        .add("systemMessage", systemMessage)
        .add("userMessage", userMessage)
        .toString();
  }
}
