package model;

import com.fasterxml.jackson.databind.JsonNode;

public class EventRequestParam {

  private String eventName;
  private String instantorRequestUid;
  private JsonNode eventBody;

  public EventRequestParam(JsonNode eventBody, String eventName, String instantorRequestUid) {
    this.eventName = eventName;
    this.instantorRequestUid = instantorRequestUid;
    this.eventBody = eventBody;
  }

  public JsonNode getEventBody() {
    return eventBody;
  }

  public EventRequestParam setEventBody(JsonNode eventBody) {
    this.eventBody = eventBody;
    return this;
  }

  public EventRequestParam() {
  }

  public String getEventName() {
    return eventName;
  }

  public void setEventName(String eventName) {
    this.eventName = eventName;
  }

  public String getInstantorRequestUid() {
    return instantorRequestUid;
  }

  public void setInstantorRequestUid(String instantorRequestUid) {
    this.instantorRequestUid = instantorRequestUid;
  }

}
