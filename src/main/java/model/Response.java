
package model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "routes",
    "fields",
    "jsonSchema",
    "experiments",
    "flowName"
})
public class Response {

  @JsonProperty("step")
  private String name;
  @JsonProperty("routes")
  private List<Object> routes = null;
  @JsonProperty("fields")
  private Map<String, Object> fields;
  @JsonProperty("jsonSchema")
  private String jsonSchema;
  @JsonProperty("experiments")
  private List<String> experiments = null;
  @JsonProperty("flowName")
  private String flowName;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();

  /**
   * No args constructor for use in serialization
   */
  public Response() {
  }

  /**
   *
   * @param experiments
   * @param routes
   * @param name
   * @param jsonSchema
   * @param flowName
   * @param fields
   */
  public Response(String name, List<Object> routes, Map<String, Object> fields, String jsonSchema,
      List<String> experiments,
      String flowName) {
    super();
    this.name = name;
    this.routes = routes;
    this.fields = fields;
    this.jsonSchema = jsonSchema;
    this.experiments = experiments;
    this.flowName = flowName;
  }

  @JsonProperty("name")
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("routes")
  public List<Object> getRoutes() {
    return routes;
  }

  @JsonProperty("routes")
  public void setRoutes(List<Object> routes) {
    this.routes = routes;
  }

  @JsonProperty("fields")
  public Map<String, Object> getFields() {
    return fields;
  }

  @JsonProperty("fields")
  public void setFields(Map<String, Object> fields) {
    this.fields = fields;
  }

  @JsonProperty("jsonSchema")
  public String getJsonSchema() {
    return jsonSchema;
  }

  @JsonProperty("jsonSchema")
  public void setJsonSchema(String jsonSchema) {
    this.jsonSchema = jsonSchema;
  }

  @JsonProperty("experiments")
  public List<String> getExperiments() {
    return experiments;
  }

  @JsonProperty("experiments")
  public void setExperiments(List<String> experiments) {
    this.experiments = experiments;
  }

  @JsonProperty("flowName")
  public String getFlowName() {
    return flowName;
  }

  @JsonProperty("flowName")
  public void setFlowName(String flowName) {
    this.flowName = flowName;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this).append("name", name).append("routes", routes).append("fields", fields)
        .append("jsonSchema", jsonSchema).append("experiments", experiments).append("flowName", flowName)
        .append("additionalProperties", additionalProperties).toString();
  }

}
