package ui;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import log.TextAreaWriter;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.OutputStreamAppender;
import org.apache.logging.log4j.core.filter.ThresholdFilter;
import org.apache.logging.log4j.core.layout.PatternLayout;
import start.Proxy;
import start.Start;

public class Config {

  @FXML
  private ListView<String> mode;

  @FXML
  private CheckBox instantorCheck;

  @FXML
  private Button start;

  @FXML
  private TextArea logs;

  @FXML
  private ListView<String> envOption;
  private Thread thread = new Thread();

  List<Thread> threadList = new ArrayList<>();

  private static int id = 0;
  private Proxy proxy;

  @FXML
  public void initialize() {
    ObservableList<String> items = FXCollections.observableArrayList("MANUAL", "FILL PERSONAL DATA",
        "EMPLOYMENT INFO", "BANK ACCOUNT INFO", "CALCULATOR", "UPLOAD DOCUMENTS", "AUTO");
    mode.setItems(items);

    ObservableList<String> items1 = FXCollections.observableArrayList("TEST", "RELEASE", "PROD");
    envOption.setItems(items1);

    TextAreaWriter.textArea = logs;
    LoggerContext context = (LoggerContext) LogManager.getContext(false);
    OutputStreamAppender appender = OutputStreamAppender.newBuilder().setName("area").setFilter(
        ThresholdFilter.createFilter(Level.INFO, null, null))
        .setLayout(PatternLayout.newBuilder().withPattern("%d{yy/MM/dd HH:mm:ss} %p %c{2}: %m%n").build())
        .setTarget(new TextAreaWriter())
        .build();
    context.getConfiguration()
        .addAppender(appender);

    appender.start();

    proxy = new Proxy();
    Proxy.INSTANCE = proxy;

    context.getRootLogger().addAppender(appender);
  }


  @FXML
  private void startPassReg() {

    synchronized (threadList) {

      Start start = new Start(instantorCheck.isSelected(), mode.getSelectionModel().getSelectedIndex(),
          envOption.getSelectionModel().getSelectedIndex(), proxy.getSeleniumProxy(), id);
      Thread thread = new Thread(start::start);
      threadList.add(thread);
      proxy.addStart(start, id);
      id++;
      thread.setDaemon(true);
      thread.start();

    }

  }
}
